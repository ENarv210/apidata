package com.ericnarvaez.stockdata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

/**
 * This Activity will display when the TickerProfile
 * is not returning with a code of 200.
 */
class APIUnavailable : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.api_unavailable)
    }
}