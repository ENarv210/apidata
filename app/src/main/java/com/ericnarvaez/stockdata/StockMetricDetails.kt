package com.ericnarvaez.stockdata

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ericnarvaez.stockdata.databinding.BasicStockMetricsBinding
import com.ericnarvaez.stockdata.restObjects.BasicStockMetrics

/**
 * This Activity will display the details of the
 * TickerProfile, which will then lead into other
 * API calls.
 */
class StockMetricDetails : AppCompatActivity()  {
    private var basicStockMetrics: BasicStockMetrics? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val basicStockMetricsBinding: BasicStockMetricsBinding = DataBindingUtil.setContentView(this, R.layout.basic_stock_metrics)
        basicStockMetrics = intent.getSerializableExtra("BasicStockMetrics") as? BasicStockMetrics
        Log.v("STATUS", "Received: $basicStockMetrics")
        basicStockMetricsBinding.setVariable(BR.basicStockMetrics, basicStockMetrics)
        basicStockMetricsBinding.executePendingBindings()
    }

}