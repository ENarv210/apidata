package com.ericnarvaez.stockdata

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.ericnarvaez.stockdata.restObjects.TickerProfile
import com.ericnarvaez.stockdata.retrofitControllers.RxJavaGlobals
import com.ericnarvaez.stockdata.retrofitControllers.TickerProfileController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.Serializable

/**
 * This activity is the starting Activity
 * in which the initial API TickerProfile object
 * is retrieved. This is event based using RxJava
 * the RxJava event is based upon the API response.
 * The REST data from the object is then bound to the
 * TickerProfileDetails Activity.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TickerProfileController.createRetrofitInstance(getString(R.string.finnhubBaseURL))

        /**
         * This is the main activity in which all view will stem from.
         * This is just a very simple app with nothing to fancy.
         */
        setContentView(R.layout.activity_main)

        //Set Submit Button API Execution//
        createButtonPressListener(findViewById(R.id.btn_tickerSubmit),findViewById(R.id.et_tickerSymbol))
    }

    /**
     * This function will execute the REST request to get the initial
     * Ticker data information.
     */
    private fun createButtonPressListener(button: Button, etTickerSymbol : EditText) {
        //Execute REST Service Request//
        button.setOnClickListener {
            //Setup Retrofit Instance//
            requestTickerProfileData(etTickerSymbol.text.toString()
            )
        }
    }

     private fun requestTickerProfileData(ticker: String) {
         RxJavaGlobals.compositeDisposable?.add(
            TickerProfileController.finnHubInstance?.getTickerData(ticker)
            !!.observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            //OnComplete, onError, onNext//
            .subscribe(this::handleTickerProfileResponse,this::handleTickerProfileBadResponse,this::handleTickerProfileComplete))
    }

    /**
     * Ticker Profile Events -> onNext, onError, and onComplete action
     */
    private fun handleTickerProfileResponse(tickerProfile: TickerProfile){
        Log.v("STATUS", "Ticker Profile Received: $tickerProfile")
        /**
         * If the rest request is returned with JSON data create view,
         * otherwise if mis-typed send to API unavailable page.
         */
        val intent = Intent(this, TickerProfileDetails::class.java).apply {
            putExtra("TickerProfile", tickerProfile as Serializable)
        }
        startActivity(intent)
    }

    private fun handleTickerProfileBadResponse(t : Throwable){
        Log.v("STATUS", "Unacceptable Response Code: $t")
        startActivity(Intent(this,APIUnavailable::class.java))
    }

    private fun handleTickerProfileComplete(){
        Log.v("STATUS", "Completed")
    }

    /**
     * onDestroy clear Composite Disposable to avoid any
     * type of leaks, bad references.
     */
    override fun onDestroy() {
        super.onDestroy()
        RxJavaGlobals.compositeDisposable?.clear()
    }
}
