package com.ericnarvaez.stockdata

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.ericnarvaez.stockdata.databinding.TickerProfileBinding
import com.ericnarvaez.stockdata.restObjects.BasicStockMetrics
import com.ericnarvaez.stockdata.restObjects.TickerProfile
import com.ericnarvaez.stockdata.retrofitControllers.RxJavaGlobals
import com.ericnarvaez.stockdata.retrofitControllers.TickerProfileController
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * This Activity will display the details of the
 * TickerProfile, which will then lead into other
 * API calls.
 */
class TickerProfileDetails : AppCompatActivity()  {

    var tickerProfileContext : Context? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val tickerProfileBinding: TickerProfileBinding = DataBindingUtil.setContentView(this, R.layout.ticker_profile)
        val tickerProfile: TickerProfile? = intent.getSerializableExtra("TickerProfile") as? TickerProfile

        Log.v("STATUS", "Received: $tickerProfile")
        tickerProfileBinding.setVariable(BR.tickerProfile, tickerProfile)
        tickerProfileBinding.tickerProfileDetails = TickerProfileDetails()
        tickerProfileBinding.executePendingBindings()
    }

    fun requestBasicStockMetrics(ticker : String?, context: Context?) {
        Log.v("STATUS", "requestBasicStockMetrics : $ticker : $context")
        this.tickerProfileContext = context

        RxJavaGlobals.compositeDisposable?.add(
             TickerProfileController.finnHubInstance?.getBasicStockMetrics(ticker)
                !!.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    //OnComplete, onError, onNext//
                    .subscribe(
                        this::handleBasicStockMetricsResponse,
                        this::handleBasicStockMetricsBadResponse,
                        this::handleBasicStockMetricsComplete
                    )
            )
    }

    /**
     * Basic Stock Metrics Events -> onNext, onError, and onComplete action
     */
    private fun handleBasicStockMetricsResponse(basicStockMetrics: BasicStockMetrics){
        Log.v("STATUS", "Basic Stock Metrics  Received: $basicStockMetrics")

        /**
         * If the rest request is returned with JSON data create view,
         * otherwise if mis-typed send to API unavailable page.
         */
        val intent = Intent(tickerProfileContext, StockMetricDetails::class.java).apply{
            putExtra("BasicStockMetrics", basicStockMetrics)
        }
        tickerProfileContext?.startActivity(intent)
    }

    private fun handleBasicStockMetricsBadResponse(t : Throwable){
        Log.v("STATUS", "Unacceptable Response Code: $t")
        tickerProfileContext?.startActivity(Intent(tickerProfileContext,APIUnavailable::class.java))
    }

    private fun handleBasicStockMetricsComplete(){
        Log.v("STATUS", "Completed")
    }

    override fun onDestroy() {
        super.onDestroy()
        RxJavaGlobals.compositeDisposable?.clear()
    }
}