package com.ericnarvaez.stockdata.restObjects

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BasicStockMetrics (
    var metric: BasicStockMetricData?,
    var metricType : String?,
    var symbol :String?
) : Serializable{
}

data class  BasicStockMetricData (
    @SerializedName("10DayAverageTradingVolume")
    var TenDayAverageTradingVolume : String?,
    @SerializedName("13WeekPriceReturnDaily")
    var ThirteenWeekPriceTradingVolume : String?,
    @SerializedName("26WeekPriceReturnDaily")
    var TwentySixWeekPriceTradingVolume : String?,
    @SerializedName("3MonthAverageTradingVolume")
    var ThreeMonthTradingVolume : String?,
    @SerializedName("52WeekHigh")
    var FiftyTwoWeekHigh : String?,
    @SerializedName("52WeekHighDate")
    var FiftyTwoWeekHighDate : String?,
    @SerializedName("52WeekLow")
    var FiftyTwoWeekLow : String?,
    @SerializedName("52WeekLowDate")
    var FiftyTwoWeekLowDate : String?,
    @SerializedName("52WeekPriceReturnDaily")
    var FiftyTwoPriceReturnDaily : String?,
    @SerializedName("5DayPriceReturnDaily")
    var FiveDayPriceReturnDaily : String?,
    var beta : String?,
    var marketCapitalization : String?,
    var monthToDatePriceReturnDaily : String?,
    @SerializedName("priceRelativeToS&P50013Week")
    var SandP50013Week : String?,
    @SerializedName("priceRelativeToS&P50026Week")
    var SandP50026Week : String?,
    @SerializedName("priceRelativeToS&P5004Week")
    var SandP5004Week : String?,
    @SerializedName("priceRelativeToS&P50052Week")
    var SandP50052Week : String?,
    @SerializedName("priceRelativeToS&P500Ytd")
    var SandP500Ytd : String?,
    var yearToDatePriceReturnDaily :String?
) : Serializable{
}