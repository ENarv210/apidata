package com.ericnarvaez.stockdata.interfaces

import com.ericnarvaez.stockdata.restObjects.BasicStockMetrics
import com.ericnarvaez.stockdata.restObjects.TickerProfile
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface FinnhubAPI {
    /**
     * This function is for calling the Finnhub Ticker Company Profile
     * API.
     */
    @GET("stock/profile2?&token=brk3rhnrh5r9g3otii9g")
    fun getTickerData(@Query("symbol") symbol : String?) : Observable<TickerProfile>

    @GET("stock/metric?&metric=price&token=brk3rhnrh5r9g3otii9g")
    fun getBasicStockMetrics(@Query("symbol") symbol : String?) : Observable<BasicStockMetrics>
}