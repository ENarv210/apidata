package com.ericnarvaez.stockdata.retrofitControllers

import io.reactivex.disposables.CompositeDisposable

object RxJavaGlobals {
    var compositeDisposable: CompositeDisposable = CompositeDisposable()
}