package com.ericnarvaez.stockdata.restObjects

import java.io.Serializable

data class TickerProfile(
    var country : String?,
    var currency : String?,
    var exchange : String?,
    var ipo : String?,
    var marketCapitalization : String?,
    var name : String?,
    var phone : String?,
    var shareOutstanding : String?,
    var ticker : String?,
    var weburl : String?,
    var logo : String?,
    var finnhubIndustry : String?
) :Serializable {}