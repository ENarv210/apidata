package com.ericnarvaez.stockdata.retrofitControllers

import android.util.Log
import com.ericnarvaez.stockdata.interfaces.FinnhubAPI
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object TickerProfileController {
    var finnHubInstance: FinnhubAPI? = null
    
    fun createRetrofitInstance(retrofitBaseUrl: String) {
        Log.v("STATUS", "Base URL: $retrofitBaseUrl")

        this.finnHubInstance = Retrofit.Builder()
            .baseUrl(retrofitBaseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(FinnhubAPI::class.java)
    }
}



